﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DirectShowLib;
using System.Runtime.InteropServices;
using System.Threading;

namespace DirectShowLibLab
{
	public class VideoWindowHandler : IDisposable
	{
		private CaptureGraphBuilderHelper _helper;
		private IntPtr _parentWindowHandle;

		public IVideoWindow VideoWindow { get; private set; }
		public IMediaControl MediaControl { get; private set; }
		public IMediaEventEx MediaEvent { get; private set; }
		
		public const int WM_GRAPHNOTIFY = 0x8000 + 1;

		public VideoWindowHandler(
			CaptureGraphBuilderHelper helper, 
			IntPtr parentWindowHandle)
		{
			_helper = helper;
			_helper.CaptureBuffer = IntPtr.Zero;
			_parentWindowHandle = parentWindowHandle;			

			helper.RenderStream();

			VideoWindow = helper.GraphBuilder as IVideoWindow;
			MediaControl = helper.GraphBuilder as IMediaControl;
			MediaEvent = helper.GraphBuilder as IMediaEventEx;

			int returnValue = VideoWindow.put_Owner(parentWindowHandle);
			DsError.ThrowExceptionForHR(returnValue);

			returnValue = MediaEvent.SetNotifyWindow(
				parentWindowHandle,
				WM_GRAPHNOTIFY,
				IntPtr.Zero);
			DsError.ThrowExceptionForHR(returnValue);
		}

		public void SetPropertyValue(
			CameraControlProperty cameraControlProperty, 
			int valueToSet)
		{
			var range = _helper.GetPropertyValueRange(cameraControlProperty);
			if ((valueToSet >= range.Item1) && (valueToSet <= range.Item2))
			{
				_helper.SetDevicePropertyValue(cameraControlProperty, valueToSet);
			}			
		}

		public int? GetPropertyValue(CameraControlProperty cameraControlProperty)
		{
			int? propertyValue = 
				_helper.GetDevicePropertyValue(cameraControlProperty);

			return propertyValue;
		}

		public Tuple<int, int> GetPropertyRange(
			CameraControlProperty cameraControlProperty)
		{
			Tuple<int, int> range =
				_helper.GetPropertyValueRange(cameraControlProperty);

			return range;
		}

		public void Resize(int width, int height)
		{
			if (VideoWindow != null)
			{
				VideoWindow.SetWindowPosition(0, 25, width, height);
			}
		}

		public int Show()
		{
			int returnValue = VideoWindow.put_WindowStyle(WindowStyle.Child);
			DsError.ThrowExceptionForHR(returnValue);
			returnValue = VideoWindow.put_Visible(OABool.True);
			DsError.ThrowExceptionForHR(returnValue);

			return returnValue;
		}

		public int Run()
		{
			int returnValue = MediaControl.Run();
			DsError.ThrowExceptionForHR(returnValue);

			return returnValue;
		}

		public IntPtr CaptureImage()
		{
			_helper.CaptureBuffer = Marshal.AllocCoTaskMem(2345239);

			try
			{
				_helper.PictureRequested = true;
			}
			catch
			{
				Marshal.FreeCoTaskMem(_helper.CaptureBuffer);
				_helper.CaptureBuffer = IntPtr.Zero;
				throw;
			}

			return _helper.CaptureBuffer;
		}

		public void Dispose()
		{
			if (MediaControl != null)
			{
				MediaControl.StopWhenReady();
			} 
			if (VideoWindow != null)
			{
				VideoWindow.put_Visible(OABool.False);
				VideoWindow.put_Owner(IntPtr.Zero);
			}
			if (MediaEvent != null)
			{
				MediaEvent.SetNotifyWindow(
					IntPtr.Zero, 
					WM_GRAPHNOTIFY, 
					IntPtr.Zero);
			}

			if (MediaControl != null)
			{
				Marshal.ReleaseComObject(MediaControl);
				MediaControl = null;
			}
			if (VideoWindow != null)
			{
				Marshal.ReleaseComObject(VideoWindow);
				VideoWindow = null;
			}

			if (MediaEvent != null)
			{
				Marshal.ReleaseComObject(MediaEvent);
				MediaEvent = null;
			}
			if (_helper != null) _helper.Dispose();
		}
	}
}
