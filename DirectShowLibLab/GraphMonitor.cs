﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DirectShowLib;

namespace DirectShowLibLab
{
	public class GraphMonitor
	{
		public CaptureGraphBuilderHelper CaptureGraphBuilderHelper { get; private set; }
		public DsROTEntry ROTEntry { get; private set; }

		public GraphMonitor(CaptureGraphBuilderHelper y)
		{
			CaptureGraphBuilderHelper = y;
		}
	}
}
