﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DirectShowLibLab
{
	public class DirectShowErrorCodes
	{
		public const uint S_OK = 0;
		public const uint VFW_S_NOPREVIEWPIN = 262782;
		public const uint E_FAIL = 2147500037;
		public const uint E_INVALIDARG = 2147942487;
		public const uint E_POINTER = 2147500035;
		public const uint VFW_E_NOT_IN_GRAPH = 2147746399;
	}
}
