﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DirectShowLib;
using System.Runtime.InteropServices.ComTypes;
using System.Runtime.InteropServices;

namespace DirectShowLibLab
{
	public class DeviceEnumerator
	{
		public DeviceEnumerator()
		{
			GetAvailableVideoInputDevices();
		}

		private List<DsDevice> _availableVideoInputDevices;
		public List<DsDevice> AvailableVideoInputDevices
		{
			get
			{
				if (_availableVideoInputDevices == null)
				{
					_availableVideoInputDevices = new List<DsDevice>();
				}

				return _availableVideoInputDevices;
			}
		}

		private void GetAvailableVideoInputDevices()
		{
			DsDevice[] devicesOfCategory =
				DsDevice.GetDevicesOfCat(FilterCategory.VideoInputDevice);
			AvailableVideoInputDevices.AddRange(devicesOfCategory);

		}
	}
}
