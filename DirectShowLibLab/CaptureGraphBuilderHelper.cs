﻿using System;
using System.Collections.Generic;
using System.Linq;
using DirectShowLib;
using System.Runtime.InteropServices;

namespace DirectShowLibLab
{
	public class CaptureGraphBuilderHelper : ISampleGrabberCB, IDisposable
	{
		public bool PictureRequested { get; set; }
		public IntPtr CaptureBuffer { get; set; }
		public DsDevice DeviceToCapture { get; private set; }
		public IFilterGraph2 StillCaptureGraphBuilder { get; private set; }
		public ICaptureGraphBuilder2 CaptureGraphBuilder { get; private set; }
		public IGraphBuilder GraphBuilder { get; private set; }
		public virtual List<CameraControlProperty> SupportedCaptureProperties
		{ get; private set; }
		public IPin StillCapturePin { get; private set; }


		public CaptureGraphBuilderHelper() 
		{
			PictureRequested = false;
		}

		public CaptureGraphBuilderHelper(DsDevice d) : this()
		{
			DeviceToCapture = d;
		}

		private IBaseFilter GetBaseFilterForDevice()
		{
			object source;
			DeviceToCapture.Mon.BindToObject(
				null,
				null,
				typeof(IBaseFilter).GUID,
				out source);
			return source as IBaseFilter;
		}

		private void InitializeSupportedCaptureProperties()
		{
			SupportedCaptureProperties = new List<CameraControlProperty>();
			foreach (CameraControlProperty currentCameraControlProperty
				in Enum.GetValues(typeof(CameraControlProperty)))
			{
				SupportedCaptureProperties.Add(currentCameraControlProperty);
			}

			foreach (CameraControlProperty currentCameraControlProperty
				in Enum.GetValues(typeof(CameraControlProperty)))
			{
				int? garbageDevicePropertyValue =
					GetDevicePropertyValue(currentCameraControlProperty);
				if (!garbageDevicePropertyValue.HasValue)
					SupportedCaptureProperties.Remove(currentCameraControlProperty);
			}
		}

		private void InitializePinsForStillCapture(IBaseFilter sourceFilter)
		{
			IBaseFilter stillCaptureFilter = null;
			StillCaptureGraphBuilder = new FilterGraph() as IFilterGraph2;
			int comReturnValue =
				StillCaptureGraphBuilder.AddSourceFilterForMoniker(
				DeviceToCapture.Mon,
				null,
				DeviceToCapture.Name,
				out stillCaptureFilter);
			DsError.ThrowExceptionForHR(comReturnValue);

			SetupCaptureWithSmartTee(stillCaptureFilter);
		}

		private void SetupCaptureWithSmartTee(IBaseFilter stillCaptureFilter)
		{
			IPin rawPin = null,
				smartTeePin = null,
				captureOutputPin = null;
			IBaseFilter smartTee = new SmartTee() as IBaseFilter;
			try
			{
				int comReturnValue =
					StillCaptureGraphBuilder.AddFilter(smartTee, "SmartTee");
				DsError.ThrowExceptionForHR(comReturnValue);

				rawPin = DsFindPin.ByCategory(
					stillCaptureFilter,
					PinCategory.Capture,
					0);
				smartTeePin = DsFindPin.ByDirection(
					smartTee,
					PinDirection.Input,
					0);
				comReturnValue = StillCaptureGraphBuilder.Connect(
					rawPin,
					smartTeePin);
				DsError.ThrowExceptionForHR(comReturnValue);

				StillCapturePin = DsFindPin.ByName(smartTee, "Preview");
				captureOutputPin = DsFindPin.ByName(smartTee, "Capture");

				SetupRemainingStack(captureOutputPin);				
			}
			finally
			{
				if (rawPin != null) Marshal.ReleaseComObject(rawPin);
				if (rawPin != smartTeePin) Marshal.ReleaseComObject(smartTeePin);
				if (rawPin != smartTee) Marshal.ReleaseComObject(smartTee);
			}
		}

		private void SetupRemainingStack(IPin captureOutputPin)
		{
			ISampleGrabber sampleGrabber = new SampleGrabber() as ISampleGrabber;
			IPin rendererInputPin = null,
				sampleInputPin = null;
			try
			{
				IBaseFilter filterForSampleGrabber =
					sampleGrabber as IBaseFilter;
				SetDefaultMediaTypeForGrabber(sampleGrabber);
				int comReturnValue = sampleGrabber.SetCallback(this, 1);
				DsError.ThrowExceptionForHR(comReturnValue);

				sampleInputPin = DsFindPin.ByDirection(
					filterForSampleGrabber,
					PinDirection.Input,
					0);
				IBaseFilter rendererFilter = 
					new VideoRendererDefault() as IBaseFilter;
				comReturnValue = StillCaptureGraphBuilder.AddFilter(
					rendererFilter, 
					"Renderer");
				DsError.ThrowExceptionForHR(comReturnValue);

				rendererInputPin = DsFindPin.ByDirection(
					rendererFilter,
					PinDirection.Input,
					0);
				comReturnValue = StillCaptureGraphBuilder.AddFilter(
					filterForSampleGrabber, 
					"Actual Grabber");

				comReturnValue = StillCaptureGraphBuilder.Connect(
					StillCapturePin,
					sampleInputPin);
				DsError.ThrowExceptionForHR(comReturnValue);
				comReturnValue = StillCaptureGraphBuilder.Connect(
					captureOutputPin,
					rendererInputPin);
				DsError.ThrowExceptionForHR(comReturnValue);
			}
			finally
			{
				if (sampleGrabber != null)
				{
					Marshal.ReleaseComObject(sampleGrabber);
					sampleGrabber = null;
				}
				if (captureOutputPin != null)
				{
					Marshal.ReleaseComObject(captureOutputPin);
					captureOutputPin = null;
				}
				if (sampleInputPin != null)
				{
					Marshal.ReleaseComObject(sampleInputPin);
					sampleInputPin = null;
				}
				if (rendererInputPin != null)
				{
					Marshal.ReleaseComObject(rendererInputPin);
					rendererInputPin = null;
				}				
			}
		}

		private void SetDefaultMediaTypeForGrabber(ISampleGrabber grabber)
		{
			AMMediaType mt = new AMMediaType
			{
				majorType = MediaType.Video,
				subType = MediaSubType.RGB24,
				formatType = FormatType.VideoInfo
			};

			int comReturnValue = grabber.SetMediaType(mt);
			DsError.ThrowExceptionForHR(comReturnValue);

			DsUtils.FreeAMMediaType(mt);
			mt = null;
		}

		public Tuple<int, int> GetPropertyValueRange(
			CameraControlProperty cameraControlProperty)
		{
			IBaseFilter f = GetBaseFilterForDevice();
			try
			{
				IAMCameraControl icc = f as IAMCameraControl;
				int min, max, defaultValue, step;
				CameraControlFlags unusedFlags;
				int propertyRangeGetResult = icc.GetRange(
					cameraControlProperty,
					out min,
					out max,
					out step,
					out defaultValue,
					out unusedFlags);
				DsError.ThrowExceptionForHR(propertyRangeGetResult);

				return Tuple.Create(min, max);
			}
			finally
			{
				if (f != null)
				{
					Marshal.ReleaseComObject(f);
				}
			}
		}

		public void SetDevicePropertyValue(
			CameraControlProperty propertyToSet,
			int valueToSet)
		{
			IBaseFilter f = GetBaseFilterForDevice();
			try
			{
				IAMCameraControl icc = f as IAMCameraControl;
				int propertySetResult = icc.Set(
					propertyToSet,
					valueToSet,
					CameraControlFlags.Manual);
				DsError.ThrowExceptionForHR(propertySetResult);
			}
			finally
			{
				if (f != null)
				{
					Marshal.ReleaseComObject(f);
				}
			}
		}

		public int? GetDevicePropertyValue(CameraControlProperty propertyValueToGet)
		{
			if (SupportedCaptureProperties == null
				||
				!SupportedCaptureProperties.Contains(propertyValueToGet))
			{
				return null;
			}

			IBaseFilter f = GetBaseFilterForDevice();
			try
			{
				int returnedPropertyValue;
				IAMCameraControl icc = f as IAMCameraControl;
				CameraControlFlags flags = CameraControlFlags.Manual;

				int propertyGetResult = icc.Get(
					propertyValueToGet,
					out returnedPropertyValue,
					out flags);
				if (propertyGetResult != DirectShowErrorCodes.S_OK)
				{
					return null;
				}
				else
				{
					return returnedPropertyValue;
				}
			}
			finally
			{
				if (f != null)
				{
					Marshal.ReleaseComObject(f);
				}
			}
		}

		public int RenderStream()
		{
			InitializeSupportedCaptureProperties();

			GraphBuilder = new FilterGraph() as IGraphBuilder;
			CaptureGraphBuilder =
				new CaptureGraphBuilder2() as ICaptureGraphBuilder2;
			CaptureGraphBuilder.SetFiltergraph(GraphBuilder);

			IBaseFilter filter = GetBaseFilterForDevice();
			GraphBuilder.AddFilter(filter, "Video Capture");

			InitializePinsForStillCapture(filter);

			int returnValue = -1;
			try
			{
				IMediaControl mediaControl = 
					CaptureGraphBuilder as IMediaControl;
				returnValue = mediaControl.Run();
				DsError.ThrowExceptionForHR(returnValue);

				//returnValue = CaptureGraphBuilder.RenderStream(
				//	PinCategory.Preview,
				//	MediaType.Video,
				//	filter,
				//	null,
				//	null);
				//DsError.ThrowExceptionForHR(returnValue);
			}
			finally
			{
				if (filter != null)
				{
					Marshal.ReleaseComObject(filter);
				}
			}

			return returnValue;
		}

		public void Dispose()
		{
			if (GraphBuilder != null)
			{
				Marshal.ReleaseComObject(GraphBuilder);
			}
			if (CaptureGraphBuilder != null)
			{
				Marshal.ReleaseComObject(CaptureGraphBuilder);
			}
		}


		int ISampleGrabberCB.BufferCB(
			double SampleTime, 
			IntPtr pBuffer, 
			int BufferLen)
		{
			if (PictureRequested)
			{
				PictureRequested = false;
			}

			return 0;
		}

		int ISampleGrabberCB.SampleCB(double SampleTime, IMediaSample pSample)
		{
			throw new NotImplementedException();
		}
	}
}
