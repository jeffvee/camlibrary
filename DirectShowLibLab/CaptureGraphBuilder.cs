﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DirectShowLib;

namespace DirectShowLibLab
{
	public class CaptureGraphBuilderHelper
	{
		public DsDevice DeviceToCapture { get; private set; }
		public ICaptureGraphBuilder2 CaptureGraphBuilder { get; private set; }
		public IGraphBuilder GraphBuilder { get; private set; }

		public CaptureGraphBuilderHelper(DsDevice d)
		{
			DeviceToCapture = d;

			InitializeGraph();
		}

		private void InitializeGraph()
		{
			CaptureGraphBuilder =
				new CaptureGraphBuilder2() as ICaptureGraphBuilder2;
			GraphBuilder = new FilterGraph() as IGraphBuilder;
		}

		public IBaseFilter GetBaseFilterForDevice()
		{
			object source;
			DeviceToCapture.Mon.BindToObject(
				null,
				null,
				typeof(IBaseFilter).GUID,
				out source);
			return source as IBaseFilter;
		}
	}
}
