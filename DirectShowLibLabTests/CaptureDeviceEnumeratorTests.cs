﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using DirectShowLibLab;
using DirectShowLib;

namespace DirectShowLibLabTests
{
	[TestFixture]
	public class CaptureDeviceEnumeratorTests
	{
		[Test]
		public void CanAtLeastDetectBuiltInWebcamOnJeffsLaptopTest()
		{
			var x = new DeviceEnumerator();

			int numberOfDevices = x.AvailableVideoInputDevices.Count;

			Assert.That(numberOfDevices, Is.AtLeast(1));
		}

		[Test]
		public void CanGetBaseFilterForDeviceTest()
		{
			var x = new DeviceEnumerator();

			IBaseFilter f = x.GetBaseFilterForDevice(x.AvailableVideoInputDevices[0]);

			Assert.That(f, Is.Not.Null);
		}
	}
}
