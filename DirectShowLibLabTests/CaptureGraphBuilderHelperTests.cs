﻿using System;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using DirectShowLibLab;
using Moq;
using DirectShowLib;

namespace DirectShowLibLabTests
{
	[TestFixture]
	public class CaptureGraphBuilderHelperTests
	{
		[Test]
		public void CanRenderStreamTest()
		{
			var x = new DeviceEnumerator();
			using (var y =
				new CaptureGraphBuilderHelper(x.AvailableVideoInputDevices[0]))
			{
				int returnValue = y.RenderStream();

				Assert.That(returnValue, Is.GreaterThanOrEqualTo(0));
			}
		}

		[Test]
		public void CanGetSupportedCapturePropertiesTest()
		{
			var x = new DeviceEnumerator();
			using (var y =
				new CaptureGraphBuilderHelper(x.AvailableVideoInputDevices[0]))
			{
				y.RenderStream();
				var supportedProperties = y.SupportedCaptureProperties;

				Assert.That(supportedProperties, Is.Not.Null);
				Assert.That(supportedProperties.Count, Is.GreaterThan(0));
			}
		}

		[Test]
		public void GetNullForUnsupportedPropertyTest()
		{
			List<CameraControlProperty> fakeListOfSupportedProperties =
				new List<CameraControlProperty> 
				{
					CameraControlProperty.Zoom
				};
			Mock<CaptureGraphBuilderHelper> mockHelper =
				new Mock<CaptureGraphBuilderHelper>();
			mockHelper.Setup(x => x.SupportedCaptureProperties)
				.Returns(fakeListOfSupportedProperties);

			int? returnedPropertyValue = mockHelper.Object.GetDevicePropertyValue
				(CameraControlProperty.Tilt);

			Assert.That(returnedPropertyValue, Is.Null);
		}

		[Test]
		public void GetPropertyValueForSupportedPropertyTest()
		{
			var x = new DeviceEnumerator();
			using (var y =
				new CaptureGraphBuilderHelper(x.AvailableVideoInputDevices[0]))
			{
				y.RenderStream();
				int? returnedPropertyValue = 
					y.GetDevicePropertyValue(CameraControlProperty.Zoom);

				Assert.That(returnedPropertyValue, Is.Not.Null);
				Assert.That(returnedPropertyValue, Is.Not.EqualTo(0));
			}
		}

		[Test]
		public void CanSetPropertyValueTest()
		{
			var x = new DeviceEnumerator();
			using (var y =
				new CaptureGraphBuilderHelper(x.AvailableVideoInputDevices[0]))
			{
				y.RenderStream();
				y.SetDevicePropertyValue(CameraControlProperty.Zoom, 2);
			}
		}

		[Test]
		public void CanGetPropertyRangeTest()
		{
			var x = new DeviceEnumerator();
			using (var y =
				new CaptureGraphBuilderHelper(x.AvailableVideoInputDevices[0]))
			{
				y.RenderStream();
				Tuple<int, int> range = 
					y.GetPropertyValueRange(CameraControlProperty.Zoom);

				Assert.That(range, Is.Not.Null);
				Assert.That(range.Item1, Is.EqualTo(0));
				Assert.That(range.Item2, Is.GreaterThan(0));
			}
		}

		[Test]
		public void CanGetStillPinTest()
		{
			var x = new DeviceEnumerator();
			using (var y =
				new CaptureGraphBuilderHelper(x.AvailableVideoInputDevices[0]))
			{
				y.RenderStream();

				IPin stillCapturePin = y.StillCapturePin;

				Assert.That(stillCapturePin, Is.Not.Null);
			}
		}
	}
}
