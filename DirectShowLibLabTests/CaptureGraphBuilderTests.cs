﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using DirectShowLibLab;
using DirectShowLib;

namespace DirectShowLibLabTests
{
	[TestFixture]
	public class CaptureGraphBuilderTests
	{
		[Test]
		public void CanInstantiateTest()
		{
			DsDevice d = new DsDevice(null);
			
			var x = new CaptureGraphBuilderHelper(d);

			Assert.That(x, Is.Not.Null);
		}

		[Test]
		public void CanBuildCaptureGraph()
		{

		}

		[Test]
		public void CanGetBaseFilterForDeviceTest()
		{
			var x = new DeviceEnumerator();
			var y = new CaptureGraphBuilderHelper(x.AvailableVideoInputDevices[0]);
			
			IBaseFilter f = y.GetBaseFilterForDevice();

			Assert.That(f, Is.Not.Null);
		}
	}
}
