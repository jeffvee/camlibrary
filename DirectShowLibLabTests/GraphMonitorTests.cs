﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using DirectShowLibLab;

namespace DirectShowLibLabTests
{
	[TestFixture]
	public class GraphMonitorTests
	{
		[Test]
		public void CanInstantiateTest()
		{
			var z = new DeviceEnumerator();
			var y = new CaptureGraphBuilderHelper(z.AvailableVideoInputDevices[0]);
			
			var x = new GraphMonitor(y);

			Assert.That(x, Is.Not.Null);
		}
	}
}
