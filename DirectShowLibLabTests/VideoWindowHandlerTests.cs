﻿using System;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using DirectShowLibLab;
using DirectShowLib;

namespace DirectShowLibLabTests
{
	[TestFixture]	
	public class VideoWindowHandlerTests
	{ 
		[Test]
		public void CanShowWindowTest()
		{
			var x = new DeviceEnumerator();
			var y = new CaptureGraphBuilderHelper(x.AvailableVideoInputDevices[0]);
			IntPtr parentWindowHandle = IntPtr.Zero;
			using (var z = new VideoWindowHandler(y, parentWindowHandle))
			{
				int returnValue = z.Show();

				Assert.That(returnValue, Is.GreaterThanOrEqualTo(0));
			}
		}

		[Test]
		public void CanRunWindowTest()
		{
			var x = new DeviceEnumerator();
			var y = new CaptureGraphBuilderHelper(x.AvailableVideoInputDevices[0]);
			IntPtr parentWindowHandle = IntPtr.Zero;
			using (var z = new VideoWindowHandler(y, parentWindowHandle))
			{
				z.Show();

				int returnValue = z.Run();

				Assert.That(returnValue, Is.GreaterThanOrEqualTo(0));			
			}
		}

		[Test]
		public void SettingPropertyValueOutsideOfRangeDoesNothingTest()
		{
			var x = new DeviceEnumerator();
			var y = new CaptureGraphBuilderHelper(x.AvailableVideoInputDevices[0]);
			IntPtr parentWindowHandle = IntPtr.Zero;
			using (var z = new VideoWindowHandler(y, parentWindowHandle))
			{
				z.Show();
				z.Run();
				int? currentPropertyValue = 
					z.GetPropertyValue(CameraControlProperty.Zoom);
				
				z.SetPropertyValue(CameraControlProperty.Zoom, Int16.MinValue);

				int? propertyValueAfterOutOfRangeSet =
					z.GetPropertyValue(CameraControlProperty.Zoom);
				Assert.That(propertyValueAfterOutOfRangeSet, 
					Is.EqualTo(currentPropertyValue));
			}
		}

		[Test]
		public void CanSetCaptureDevicePropertyTest()
		{
			var x = new DeviceEnumerator();
			var y = new CaptureGraphBuilderHelper(x.AvailableVideoInputDevices[0]);
			IntPtr parentWindowHandle = IntPtr.Zero;
			using (var z = new VideoWindowHandler(y, parentWindowHandle))
			{
				z.Show();
				z.Run();

				z.SetPropertyValue(CameraControlProperty.Zoom, 2);

				int? propertyValue = z.GetPropertyValue(CameraControlProperty.Zoom);
				Assert.That(propertyValue, Is.EqualTo(2));
			}
		}

		[Test]
		public void CanGetCaptureDevicePropertyTest()
		{
			var x = new DeviceEnumerator();
			var y = new CaptureGraphBuilderHelper(x.AvailableVideoInputDevices[0]);
			IntPtr parentWindowHandle = IntPtr.Zero;
			using (var z = new VideoWindowHandler(y, parentWindowHandle))
			{
				z.Show();
				z.Run();

				int? propertyValue = z.GetPropertyValue(CameraControlProperty.Zoom);

				Assert.That(propertyValue, Is.GreaterThanOrEqualTo(0));
			}
		}

		[Test]
		public void CanGetPropertyValueRangeTest()
		{
			var x = new DeviceEnumerator();
			var y = new CaptureGraphBuilderHelper(x.AvailableVideoInputDevices[0]);
			IntPtr parentWindowHandle = IntPtr.Zero;
			using (var z = new VideoWindowHandler(y, parentWindowHandle))
			{
				z.Show();
				z.Run();

				Tuple<int, int> propertyRange = 
					z.GetPropertyRange(CameraControlProperty.Zoom);

				Assert.That(propertyRange, Is.Not.Null);
				Assert.That(propertyRange.Item1, Is.EqualTo(0));
				Assert.That(propertyRange.Item2, Is.GreaterThan(0));
			}
		}
	}
}
