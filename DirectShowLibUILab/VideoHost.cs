﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using DirectShowLibLab;
using DirectShowLib;
using System.Runtime.InteropServices;

namespace DirectShowLibUILab
{
	public partial class VideoHost : Form
	{
		readonly VideoWindowHandler _videoHandler = null;
		IntPtr _captureBuffer = IntPtr.Zero;

		public VideoHost()
		{
			InitializeComponent();

			var x = new DeviceEnumerator();
			var y = new CaptureGraphBuilderHelper(x.AvailableVideoInputDevices[0]);
			_videoHandler = new VideoWindowHandler(y, Handle);
			
			_videoHandler.Show();
			_videoHandler.Run();
			VideoHost_Resize(null, null);
		}

		protected override void OnClosed(EventArgs e)
		{
			if (_videoHandler != null) _videoHandler.Dispose();

			base.OnClosed(e);
		}

		private void VideoHost_Resize(object sender, EventArgs e)
		{
			_videoHandler.Resize(Width, Height);
		}

		private void zoomOut_Click(object sender, EventArgs e)
		{
			int? currentZoomValue = 
				_videoHandler.GetPropertyValue(CameraControlProperty.Zoom);
			_videoHandler.SetPropertyValue(CameraControlProperty.Zoom, 
				currentZoomValue.Value - 1);			
		}

		private void zoomIn_Click(object sender, EventArgs e)
		{
			int? currentZoomValue =
				_videoHandler.GetPropertyValue(CameraControlProperty.Zoom);
			_videoHandler.SetPropertyValue(CameraControlProperty.Zoom,
				currentZoomValue.Value + 1);
		}

		private void panLeft_Click(object sender, EventArgs e)
		{
			int? currentPanValue =
				_videoHandler.GetPropertyValue(CameraControlProperty.Pan);
			_videoHandler.SetPropertyValue(CameraControlProperty.Pan,
				currentPanValue.Value - 1);
		}

		private void panRight_Click(object sender, EventArgs e)
		{
			int? currentPanValue =
				_videoHandler.GetPropertyValue(CameraControlProperty.Pan);
			_videoHandler.SetPropertyValue(CameraControlProperty.Pan,
				currentPanValue.Value + 1);
		}

		private void takePicture_Click(object sender, EventArgs e)
		{
			if (_captureBuffer != IntPtr.Zero)
			{
				Marshal.FreeCoTaskMem(_captureBuffer);
				_captureBuffer = IntPtr.Zero;
			}

			_captureBuffer = _videoHandler.CaptureImage();
		}		
	}
}
