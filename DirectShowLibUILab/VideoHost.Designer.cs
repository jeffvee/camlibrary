﻿namespace DirectShowLibUILab
{
	partial class VideoHost
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.zoomOut = new System.Windows.Forms.Button();
			this.zoomIn = new System.Windows.Forms.Button();
			this.panLeft = new System.Windows.Forms.Button();
			this.panRight = new System.Windows.Forms.Button();
			this.takePicture = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// zoomOut
			// 
			this.zoomOut.Location = new System.Drawing.Point(0, 0);
			this.zoomOut.Name = "zoomOut";
			this.zoomOut.Size = new System.Drawing.Size(75, 23);
			this.zoomOut.TabIndex = 0;
			this.zoomOut.Text = "Zoom Out";
			this.zoomOut.UseVisualStyleBackColor = true;
			this.zoomOut.Click += new System.EventHandler(this.zoomOut_Click);
			// 
			// zoomIn
			// 
			this.zoomIn.Location = new System.Drawing.Point(81, 0);
			this.zoomIn.Name = "zoomIn";
			this.zoomIn.Size = new System.Drawing.Size(75, 23);
			this.zoomIn.TabIndex = 1;
			this.zoomIn.Text = "Zoom In";
			this.zoomIn.UseVisualStyleBackColor = true;
			this.zoomIn.Click += new System.EventHandler(this.zoomIn_Click);
			// 
			// panLeft
			// 
			this.panLeft.Location = new System.Drawing.Point(205, 0);
			this.panLeft.Name = "panLeft";
			this.panLeft.Size = new System.Drawing.Size(75, 23);
			this.panLeft.TabIndex = 2;
			this.panLeft.Text = "Pan Left";
			this.panLeft.UseVisualStyleBackColor = true;
			this.panLeft.Click += new System.EventHandler(this.panLeft_Click);
			// 
			// panRight
			// 
			this.panRight.Location = new System.Drawing.Point(286, 0);
			this.panRight.Name = "panRight";
			this.panRight.Size = new System.Drawing.Size(75, 23);
			this.panRight.TabIndex = 3;
			this.panRight.Text = "Pan Right";
			this.panRight.UseVisualStyleBackColor = true;
			this.panRight.Click += new System.EventHandler(this.panRight_Click);
			// 
			// takePicture
			// 
			this.takePicture.Location = new System.Drawing.Point(404, 0);
			this.takePicture.Name = "takePicture";
			this.takePicture.Size = new System.Drawing.Size(75, 23);
			this.takePicture.TabIndex = 4;
			this.takePicture.Text = "Take Pic";
			this.takePicture.UseVisualStyleBackColor = true;
			this.takePicture.Click += new System.EventHandler(this.takePicture_Click);
			// 
			// VideoHost
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(578, 389);
			this.Controls.Add(this.takePicture);
			this.Controls.Add(this.panRight);
			this.Controls.Add(this.panLeft);
			this.Controls.Add(this.zoomIn);
			this.Controls.Add(this.zoomOut);
			this.Name = "VideoHost";
			this.Text = "VideoHost";
			this.Resize += new System.EventHandler(this.VideoHost_Resize);
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Button zoomOut;
		private System.Windows.Forms.Button zoomIn;
		private System.Windows.Forms.Button panLeft;
		private System.Windows.Forms.Button panRight;
		private System.Windows.Forms.Button takePicture;
	}
}